﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomBase : MonoBehaviour {

	public bool moveNorth = false;
	public bool moveEast = false;
	public bool moveSouth = false;
	public bool moveWest = false;

	public GameObject northPath;
	public GameObject eastPath;
	public GameObject southPath;
	public GameObject westPath;


	// Use this for initialization
	void Start () {
        Hide();
		hidePaths();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public List<RoomBase> FindNeighboringRooms()
    {
        var ret = new List<RoomBase>();
        var pos = (Vector2)transform.position;
        if (moveNorth)
        {
            var room = GetRoomAt(pos + new Vector2(0, 10));
            if (room)
            {
                ret.Add(room);
            }
        }
        if (moveEast)
        {
            var room = GetRoomAt(pos + new Vector2(10, 0));
            if (room)
            {
                ret.Add(room);
            }
        }
        if (moveSouth)
        {
            var room = GetRoomAt(pos + new Vector2(0, -10));
            if (room)
            {
                ret.Add(room);
            }
        }
        if (moveWest)
        {
            var room = GetRoomAt(pos + new Vector2(-10, 0));
            if (room)
            {
                ret.Add(room);
            }
        }
        return ret;
    }

    private RoomBase GetRoomAt(Vector2 position)
    {
        var room = Physics2D.OverlapPoint(position);
        if (room)
        {
            return room.GetComponent<RoomBase>();
        }
        else
        {
            return null;
        }
    }

	public void hidePaths() {
		if(!moveNorth){
			Destroy(northPath);
		}
		if(!moveEast){
			Destroy(eastPath);
		}
		if(!moveSouth){
			Destroy(southPath);
		}
		if(!moveWest){
			Destroy(westPath);
		}
	}

    public void Hide()
    {
        foreach (Transform child in transform)
        {
            child.gameObject.layer = LayerMask.NameToLayer("Invisible");
        }
        gameObject.layer = LayerMask.NameToLayer("Invisible");
    }

    public void Show()
    {
        foreach (Transform child in transform)
        {
            child.gameObject.layer = LayerMask.NameToLayer("Default");
        }
        gameObject.layer = LayerMask.NameToLayer("Default");
    }
}
