﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationEventArgs : EventArgs
{
	public rotationDirection rotDir;
	public RotationEventArgs(rotationDirection dir)
	{
		rotDir = dir;
	}
};

public class PlayerBase : MonoBehaviour
{
	public float MoveSpeed = 10;
	private bool Moving = false;
	private RoomBase moveTarget;

	private rotationDirection _currentRotation = rotationDirection.NORTH;
	public rotationDirection CurrentRotation
	{
		set
		{
			_currentRotation = value;
			OnRaiseRotationEvent(new RotationEventArgs(value));
		}
		get
		{
			return _currentRotation;
		}
	}
	public event EventHandler<RotationEventArgs> RotationEventHandler;

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		if (!Moving && !GameManager.Instance.disableMovement)
		{
			var room = GetCurrenctRoom();
			if (room)
			{
				int directionInput = -1;
				if (Input.GetKey("w"))
				{
					directionInput = 0;
				}
				else if (Input.GetKey("d"))
				{
					directionInput = 1;
				}
				else if (Input.GetKey("s"))
				{
					directionInput = 2;
				}
				else if (Input.GetKey("a"))
				{
					directionInput = 3;
				}


				if (directionInput >= 0)
				{
					int realDirection = GetRealDirection(directionInput);
					RoomBase targetRoom = null;
					if (realDirection == 0)
					{
						setRotation(90);
						if (room.moveNorth)
						{
							targetRoom = GetNaboerRoom(rotationDirection.NORTH);
						}
					}
					else if (realDirection == 1)
					{
						setRotation(0);
						if (room.moveEast)
						{
							targetRoom = GetNaboerRoom(rotationDirection.EAST);
						}
					}
					else if (realDirection == 2)
					{
						setRotation(270);
						if (room.moveSouth)
						{
							targetRoom = GetNaboerRoom(rotationDirection.SOUTH);
						}
					}
					else if (realDirection == 3)
					{
						setRotation(180);
						if (room.moveWest)
						{
							targetRoom = GetNaboerRoom(rotationDirection.WEST);
						}
					}

					if (targetRoom)
					{
						VictoryRoom victoryRoom = targetRoom.GetComponent<VictoryRoom>();
						if (victoryRoom && !victoryRoom.isUnlocked())
						{

						}
						else
						{
							moveTarget = targetRoom;
							Moving = true;
							startOfMove(moveTarget);
						}
					}
				}
			}
		}
		Animator animator = GetComponent<Animator>();
		if (Moving)
		{
			animator.enabled = true;
			float step = MoveSpeed * Time.deltaTime;
			var targetPosition = moveTarget.transform.position;
			targetPosition.z = transform.position.z;
			transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);

			if (transform.position == targetPosition)
			{
				endOfMove(moveTarget);
			}
		}
		else
		{
			animator.Play("", 0, 0f);
		}
	}

	private void setRotation(float newRotation)
	{
		transform.rotation = Quaternion.Euler(0, 0, newRotation);
	}
	public virtual void OnRaiseRotationEvent(RotationEventArgs args)
	{
		EventHandler<RotationEventArgs> handler = RotationEventHandler;
		if (handler != null)
		{
			handler(this, args);
		}
	}

	public RoomBase GetCurrenctRoom()
	{
		return GetRoomAt(transform.position);
	}

	private RoomBase GetNaboerRoom(rotationDirection direction)
	{
		var targetPosition = transform.position;
		switch (direction)
		{
			case rotationDirection.NORTH:
				targetPosition.y += 10;
				break;
			case rotationDirection.EAST:
				targetPosition.x += 10;
				break;
			case rotationDirection.SOUTH:
				targetPosition.y -= 10;
				break;
			case rotationDirection.WEST:
				targetPosition.x -= 10;
				break;
		}
		return GetRoomAt(targetPosition);
	}

    public RoomBase GetRoomAt(Vector2 position)
	{
		var room = Physics2D.OverlapPoint(position);
		if (room)
		{
			return room.GetComponent<RoomBase>();
		}
		else
		{
			return null;
		}
	}

	private int GetRealDirection(int direction)
	{
		int newDirection = direction;
		switch (_currentRotation)
		{
			case rotationDirection.NORTH:
				newDirection += 0;
				break;
			case rotationDirection.EAST:
				newDirection += 1;
				break;
			case rotationDirection.SOUTH:
				newDirection += 2;
				break;
			case rotationDirection.WEST:
				newDirection += 3;
				break;
		}
		return newDirection % 4;
	}

    public void SetRenderingOfNeighborsAt(Vector3 pos, bool enabled)
    {
        var currentRoom = GetRoomAt(pos);
        var curNeighbors = currentRoom.FindNeighboringRooms();
        foreach (var neighbor in curNeighbors)
        {
            if (enabled)
            {
                neighbor.Show();
            }
            else
            {
                neighbor.Hide();
            }
        }
    }

	public void startOfMove(RoomBase targetRoom) {
		SetRenderingOfNeighborsAt(transform.position, false);
		GetCurrenctRoom().Hide();
		SetRenderingOfNeighborsAt(targetRoom.transform.position, true);
		targetRoom.Show();
	}

	public void endOfMove(RoomBase targetRoom) {
		Moving = false;
		RotationRoom rotationRoom = targetRoom.GetComponent<RotationRoom>();
		if (rotationRoom)
		{
			CurrentRotation = rotationRoom.direction;
		}
		TeleportRoom teleportRoom = targetRoom.GetComponent<TeleportRoom>();
		if (teleportRoom)
		{
			teleportRoom.teleportToTarget(this);
		}
		KeyRoom keyRoom = targetRoom.GetComponent<KeyRoom>();
		if (keyRoom)
		{
			keyRoom.getKey();
		}
		VictoryRoom victoryRoom = targetRoom.GetComponent<VictoryRoom>();
		if(victoryRoom){
			victoryRoom.showVictoryScreen();
		}
	}
}
