﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : Singleton<CameraManager>
{
    public GameObject CameraPrefab;
    public Camera MainCamera { get; private set; }
    private rotationDirection CurrentRotationDir;
    private rotationDirection TargetRotationDir;
    private Quaternion LastRotation;
    private Quaternion TargetRotation;
    public float TimeToRotate = 4f;
    private float RotationT;

    public AudioClip RotationSoundEffect;

    protected CameraManager() { }

    // Use this for initialization
    void Start()
    {
        RotationT = TimeToRotate;
        DontDestroyOnLoad(this);
        MainCamera = Instantiate(CameraPrefab).GetComponent<Camera>();
    }

    public void test()
    {
        var values = new rotationDirection[] { rotationDirection.EAST, rotationDirection.NORTH, rotationDirection.SOUTH, rotationDirection.WEST };
        var dir = values[Random.Range(0, 3)];
        dir = rotationDirection.SOUTH;
        print(dir);
        OnRotationEvent(this, new RotationEventArgs(dir));
    }

    private void Awake()
    {
        GameManager.Instance.Player.GetComponent<PlayerBase>().RotationEventHandler += OnRotationEvent;
    }

    // Update is called once per frame
    void Update()
    {
        var player = GameManager.Instance.Player;
        if (player != null)
        {
            // Move camera to player
            MainCamera.transform.position = GameManager.Instance.Player.transform.position;
        }
        TweenRotation();
    }

    void OnRotationEvent(object sender, RotationEventArgs args)
    {
        TargetRotationDir = args.rotDir;
		GameManager.Instance.disableMovement = true;
        AdvanceRotation();
    }

    void AdvanceRotation()
    {
        if (CurrentRotationDir != TargetRotationDir)
        {
            SoundManager.Instance.PlaySound(RotationSoundEffect);
            RotationT = 0;
            if (CurrentRotationDir > TargetRotationDir)
            {
                CurrentRotationDir = (rotationDirection)((System.Math.Abs(((int)CurrentRotationDir - 1) % 4)));
            }
            else if (CurrentRotationDir < TargetRotationDir)
            {
                CurrentRotationDir = (rotationDirection)((((int)CurrentRotationDir + 1) % 4));
            }
            LastRotation = MainCamera.transform.rotation;
            TargetRotation = RotationDirToQuaternion(CurrentRotationDir);
        } else {
			GameManager.Instance.disableMovement = false;
		}

    }

    Quaternion RotationDirToQuaternion(rotationDirection rotDir)
    {
        Quaternion rotation = Quaternion.identity;
        switch (rotDir)
        {
            case rotationDirection.NORTH:
                rotation = Quaternion.Euler(0, 0, 0);
                break;
            case rotationDirection.EAST:
                rotation = Quaternion.Euler(0, 0, -90);
                break;
            case rotationDirection.SOUTH:
                rotation = Quaternion.Euler(0, 0, 180);
                break;
            case rotationDirection.WEST:
                rotation = Quaternion.Euler(0, 0, 90);
                break;
        }
        return rotation;
    }

    void TweenRotation()
    {
        if (RotationT < TimeToRotate)
        {
            RotationT += Time.deltaTime;
            MainCamera.transform.position += new Vector3(Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f));
            if (RotationT > TimeToRotate)
            {
                RotationT = TimeToRotate;
                Invoke("AdvanceRotation", 1f);

            }
            MainCamera.transform.rotation = Quaternion.Lerp(LastRotation, TargetRotation, RotationT / TimeToRotate);
        }
    }
}
