﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public GameObject Player;
	public bool disableMovement = false;

	private int keyCount = 0;
	public int keysNeededForDoor = 4;
	public GameObject lockDoor;
	// Use this for initialization

	protected GameManager() { }
    void Start()
    {
        DontDestroyOnLoad(this); // Makes sure our manager isn't destroyed on load
                                 // Find or instantiate other managers here
                                 // Just grab an instance to make sure it is instantiated;

    }

    void Awake()
    {
        Invoke("FixVisibility", 0);
    }

    void FixVisibility()
    {
        var player = Player.GetComponent<PlayerBase>();
        var startingRoom = player.GetRoomAt(Player.transform.position);
        startingRoom.Show();
        player.SetRenderingOfNeighborsAt(Player.transform.position, true);
    }

    // Update is called once per frame
    void Update()
    {

    }

	public void addKey(){
		keyCount++;
		if(gotAllKeys()){
			Destroy(lockDoor);
		}
	}

	public bool gotAllKeys(){
		return keyCount == keysNeededForDoor;
	}
}
