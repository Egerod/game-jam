﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager>
{
    public AudioSource DefaultSource;
    public AudioClip MusicClip;
    private AudioSource MusicSource;

    protected SoundManager() { }

    // Use this for initialization
    void Start()
    {
    }

    private void Awake()
    {
        Invoke("PlayMusic", 0.0f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PlayMusic()
    {
        if(MusicSource != null)
        {
            Destroy(MusicSource);
        }
        MusicSource = Instantiate(DefaultSource, CameraManager.Instance.MainCamera.transform) as AudioSource;
        MusicSource.clip = MusicClip;
        MusicSource.volume = 1.0f;
        MusicSource.loop = true;
        MusicSource.Play();
    }

    public void PlaySound(AudioClip sound, float volume = 1.0f, Transform parent = null)
    {
        if (parent == null)
        {
            parent = CameraManager.Instance.MainCamera.transform;
        }
        AudioSource source = Instantiate(DefaultSource, CameraManager.Instance.MainCamera.transform) as AudioSource;
        source.clip = sound;
        source.volume = volume;
        source.Play();
        StartCoroutine(DeleteAudioSource(source, source.clip.length));
    }

    IEnumerator DeleteAudioSource(AudioSource obj, float Time)
    {
        yield return new WaitForSeconds(Time);
        Destroy(obj);
    }
}
