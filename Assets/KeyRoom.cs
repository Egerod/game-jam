﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyRoom : MonoBehaviour {

	private bool haveKey = true;
	public GameObject roomKeyObject;
    public AudioClip KeyPickup;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void getKey() {
		if(haveKey){
			haveKey = false;
			Destroy(roomKeyObject);
			GameManager.Instance.addKey();
            SoundManager.Instance.PlaySound(KeyPickup);
		}
	}
}
