﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportRoom : MonoBehaviour {

	public RoomBase targetRoom;
    public AudioClip TeleportSound;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void teleportToTarget(PlayerBase player){
		player.startOfMove(targetRoom);
		Vector3 targetLocation = targetRoom.transform.position;
		targetLocation.z = player.transform.position.z;
		player.transform.position = targetLocation;
        SoundManager.Instance.PlaySound(TeleportSound);
		player.endOfMove(targetRoom);
	}
}
