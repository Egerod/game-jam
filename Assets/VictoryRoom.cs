﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryRoom : MonoBehaviour {

	public GameObject VictorySplash;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public bool isUnlocked() {
		return GameManager.Instance.gotAllKeys();
	}

	public void showVictoryScreen() {
		GameManager.Instance.disableMovement = true;
		Instantiate(VictorySplash);
	}
}
